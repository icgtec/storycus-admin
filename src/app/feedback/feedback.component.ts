import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  token: string = '';
  experience: string = '';
  customer_firstname: string = '';
  product: string = '';
  logo: string = 'https://naturalave.com/design/resources/logo.png';
  formAvailable: boolean = false;
  responseData: any = [];
  formData: any = [];
  thankYou: boolean = false;
  alreadySubmitted: boolean = false;
  
  experienceData: any = {
    customer_firstname: '',
    customer_lastname: '',
    feedback_id: '',
    merchant_id: '',
    product: {
      product_id: '',
      product_name: ''
    },
    experience: '',
    files: {
      'first': { 'original': '', 'thumb': ''},
      'second': { 'original': '', 'thumb': ''},
      'third': { 'original': '', 'thumb': ''},
      'fourth': { 'original': '', 'thumb': ''},
    }
  };

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
  ) { }

  async ngOnInit() {

    this.token = await this.route.snapshot.params.token;

    if(this.token != '') {
      this.responseData = await this.api.getForm(this.token);

      if(this.responseData.status === 'success') {
        this.formAvailable = true;
        this.formData = this.responseData.data;
        this.logo = this.formData.logo;
        this.formData.request_data = JSON.parse(this.formData.request_data);

        if(this.formData.response_data.length > 5) {
          this.alreadySubmitted = true;
        }

        this.experienceData.customer_firstname = this.formData.customer_firstname;
        this.experienceData.customer_lastname = this.formData.customer_lastname;
        this.experienceData.feedback_id = this.formData.feedback_id;
        this.experienceData.merchant_id = this.formData.request_data.merchant_id;
        this.experienceData.product = this.formData.request_data.products[0];


      } 

    }


  }


  async upload(event: any, ref: string) {

    const file:File = event.target.files[0];

    if (file) {

        let fileName = file.name;

        const formData = new FormData();

        formData.append("thumbnail", file);

        let resp: any = await this.api.uploadFile(formData);

        if(resp.status === 'success') {

          let fileToUpdate = { 'original': '', 'thumb': ''};
          if(ref === 'first') {
            fileToUpdate = this.experienceData.files.first;
          }
          if(ref === 'second') {
            fileToUpdate = this.experienceData.files.second;
          }
          if(ref === 'third') {
            fileToUpdate = this.experienceData.files.third;
          }
          if(ref === 'fourth') {
            fileToUpdate = this.experienceData.files.fourth;
          }

          fileToUpdate.original = resp.uploadedFile.filename;
          fileToUpdate.thumb = 'https://api.icgtec.com/naturalave/uploads/'+fileToUpdate.original;

        }

    }


  }

  async submitExperience() {


    console.log(this.experienceData);

    let resp: any = await this.api.publishFeedback(this.experienceData);
    if (resp.status === 'success') {
      this.thankYou = true;
    }

  }

}
