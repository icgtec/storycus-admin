import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }


  async getMerchants() {


    return this.http.get('https://www.storycus.com/api/admin/merchants').toPromise();

  }

  async getCategories() {


    return this.http.get('https://www.storycus.com/api/admin/categories').toPromise();

  }

  async getFeedbacks() {


    return this.http.get('https://www.storycus.com/api/admin/feedbacks').toPromise();

  }

  async getProducts() {


    return this.http.get('https://www.storycus.com/api/admin/products').toPromise();

  }

  async addMerchant(url:string, name:string) {

    return this.http.post('https://www.storycus.com/api/admin/merchants', {url, name}).toPromise();

  }

  async addProduct(merchant_id:string, name:string, slug:string) {

    return this.http.post('https://www.storycus.com/api/admin/products', {merchant_id, name, slug}).toPromise();

  }

  async getFeed(productId: any, clientId: any) {

    return this.http.get('https://www.storycus.com/api/product').toPromise();
 
  }

  async getForm(token: string) {

    return this.http.get('https://www.storycus.com/api/feedback/'+token).toPromise();

  }

  async uploadFile(formData: any) {

    return this.http.post('https://www.storycus.com/api/uploadfile', formData).toPromise();

  }

  async publishFeedback(data: any) {

    return this.http.post('https://www.storycus.com/api/feedback', data).toPromise();

  }
}
