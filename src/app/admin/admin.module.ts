import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { FormsModule } from '@angular/forms';
import { MyFilterPipe } from '../pipes/filterPipe';



@NgModule({
  declarations: [
    AdminComponent,
    MyFilterPipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class AdminModule { }
