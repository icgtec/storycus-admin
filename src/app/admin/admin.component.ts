import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  merchants: any = [];
  products: any = [];
  categories: any = [];
  feedbacks: any = [];
  merchantFormOpened: boolean = false;
  productFormOpened: boolean = false;
  feedbackFormOpened: boolean = false;
  merchantTable: boolean = false;
  productTable: boolean = false;
  feedbackTable: boolean = false;
  newMerchantURL: any = '';
  newMerchantName: any = '';
  newProductName: any = '';
  newProductSlug: any = '';
  newProductMerchant: any = '';
  newFeedbackMerchant: any = '';
  newFeedbackProduct: any = '';
  newFeedbackCategory: any = '';


  constructor(
    private api: ApiService
  ) { }

  openMerchantForm() {

   
    if(this.merchantFormOpened) {
      this.merchantFormOpened = false;
    } else {
      this.merchantFormOpened = true;
    }

  }

  openProductForm() {

    if(this.productFormOpened) {
      this.productFormOpened = false;
    } else {
      this.productFormOpened = true;
    }

  }

  openFeedbackForm() {

    if(this.feedbackFormOpened) {
      this.feedbackFormOpened = false;
    } else {
      this.feedbackFormOpened = true;
    }

  }


  async addProduct() {

    let result: any = await this.api.addProduct(this.newProductMerchant, this.newProductName, this.newProductSlug);
    if(result.status === 'success') {
     this.updateProducts(); 
     this.newProductMerchant = '';
     this.newProductName = '';
     this.newProductSlug = '';
    }
    

  }

  async addMerchant() {


    let result: any = await this.api.addMerchant(this.newMerchantName, this.newMerchantURL);
    if(result.status === 'success') {
     this.updateMerchants(); 
     this.newMerchantName = '';
     this.newMerchantURL = '';
    }

  }

  async updateMerchants() {

    let merchants: any = await this.api.getMerchants();
    if (merchants.status === 'success') {
    this.merchants = merchants.data;
    }


  }

  async updateProducts() {

    let products: any = await this.api.getProducts();
    if (products.status === 'success') {
    this.products = products.data;

     

    }


  }


  async updateFeedbacks() {

    let feedbacks: any = await this.api.getFeedbacks();
    if (feedbacks.status === 'success') {
    this.feedbacks = feedbacks.data;

    for (let feedback of this.feedbacks) {

      feedback.request_data = JSON.parse(feedback.request_data);
      feedback.response_data = JSON.parse(feedback.response_data);
      console.log(feedback.response_data.merchant_id);
      if(feedback.response_data.merchant_id === undefined) {
        feedback.status = 'Waiting feedback';
      } else {
        feedback.status = 'Feedback received';
      }
    }


    

    }


  }

  async updateCategories() {

    let categories: any = await this.api.getCategories();
    if (categories.status === 'success') {
    this.categories = categories.data;
    }


  }

  async ngOnInit() {

    
    this.updateMerchants();
    this.updateProducts();
    this.updateFeedbacks();
    this.updateCategories();


  }


}
